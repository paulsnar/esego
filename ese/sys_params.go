// +build windows

package ese

type JetParam int

// Backup and Restore parameters.
const (
  JetParamAlternateDatabaseRecoveryPath JetParam = 113
  JetParamCleanupMismatchedLogFiles = 77
  JetParamDeleteOutOfRangeLogs = 52
  JetParamOSSnapshotTimeout = 82
  JetParamZeroDatabaseDuringBackup = 71
)

// Callback parameters.
const (
  JetParamDisableCallbacks JetParam = 65
  JetParamEnablePersistedCallbacks = 156
  JetParamRuntimeCallback = 73
)

// Database parameters.
const (
  JetParamCheckFormatWhenOpenFail JetParam = 44 // Obsolete since Windows Vista.
  JetParamDatabasePageSize = 64
  JetParamDBExtensionSize = 18
  JetParamEnableIndexChecking = 45
  JetParamEnableIndexCleanup = 54
  JetParamOneDatabasePerSession = 102
  JetParamEnableOnlineDefrag = 35
  JetParamPageFragment = 20
  JetParamRecordUpgradeDirtyLevel = 78
  JetParamWaypointLegacy = 153
  JetParamDefragmentSequentialBTrees = 160
  JetParamDefragmentSequentialBTreesDensityCheckFrequency = 161
  JetParamIOThrottlingTimeQuanta = 162
)

// Error handling parameters.
const (
  JetParamErrorToString JetParam = 70
  JetParamExceptionAction = 98
)

// Event log parameters.
const (
  JetParamEventLogCache JetParam = 99
  JetParamEventLoggingLevel = 51
  JetParamEventSource = 4
  JetParamEventSourceKey = 49
  JetparamNoInformationEvent = 50
)

// I/O parameters.
const (
  JetParamAccessDeniedRetryPeriod JetParam = 53
  JetParamCreatePathIfNotExist = 100
  JetParamEnableFileCache = 126
  JetParamIOPriority = 152
  JetParamOutstandingIOMax = 30
  JetParamMaxCoalesceReadSize = 164
  JetParamMaxCoalesceWriteSize = 165
  JetParamMaxCoalesceReadGapSize = 166
  JetParamMaxCoalesceWriteGapSize = 167
)

// Index parameters.
const (
  JetParamIndexTupleIncrement JetParam = 132
  JetParamIndexTupleStart = 133
  JetParamIndexTuplesLengthMax = 111
  JetParamIndexTuplesLengthMin = 110
  JetParamIndexTuplesToIndexMax = 112
  JetParamUnicodeIndexDefault = 72
)

// Informational parameters.
const (
  JetParamKeyMost JetParam = 134
  JetParamMaxColtyp = 131
  JetParamLVChunkSizeMost = 163
)

// Meta parameters.
const (
  JetParamConfiguration JetParam = 129
  JetParamEnableAdvanced = 130
)

// Resource parameters.
const (
  JetParamCachedClosedTables JetParam = 125
  JetParamDisablePerfmon = 107
  JetParamGlobalMinVerPages = 81
  JetParamMaxCursors = 8
  JetParamMaxInstances = 104
  JetParamMaxOpenTables = 6
  JetParamMaxSessions = 5
  JetParamMaxTemporaryTables = 10
  JetParamMaxVerPages = 9
  JetParamPageHintCacheSize = 101
  JetParamPreferredMaxOpenTables = 7
  JetParamPreferredVerPages = 63
  JetParamVerPageSize = 128
  JetParamVersionStoreTaskQueueMax = 105
)

// Temporary Database parameters.
const (
  JetParamEnableTempTableVersioning JetParam = 46
  JetParamPageTempDBMin = 19
  JetParamTempPath = 1
)

// Transaction Log parameters.
const (
  JetParamBaseName JetParam = 3
  JetParamCircularLog = 17
  JetParamCommitDefault = 16
  JetParamDeleteOldLogs = 48
  JetParamIgnoreLogVersion = 47
  JetParamLegacyFileNames = 136
  JetParamLogBuffers = 12
  JetParamLogCheckpointPeriod = 14
  JetParamLogFeilCreateAsynch = 69
  JetParamLogFilePath = 2
  JetParamLogFileSize = 11
  JetParamLogWaitingUserMax = 15
  JetParamRecovery = 34
  JetParamSystemPath = 0
  JetParamWaitLogFlush = 13
)
