// +build windows

package ese

// #cgo LDFLAGS: -lesent
// #include <stdlib.h>
// #include <esent.h>
import "C"

import "unsafe"

type Instance struct {
  inst C.JET_INSTANCE
}

func CreateInstance() (*Instance, JetError) {
  var inst C.JET_INSTANCE
  stat := C.JetCreateInstance(&inst, nil)
  return &Instance{ inst }, JetError(stat)
}

func (i *Instance) SetSystemParameter(p JetParam, val interface{}) JetError {
  if boolVal, ok := val.(bool); ok {
    if boolVal {
      val = 1
    } else {
      val = 0
    }
  }

  var stat C.long

  switch x := val.(type) {
  // case JetCallback: // TODO

  case uint32:
  case uint64:
  case int32:
  case int64:
  case int:
    stat = C.JetSetSystemParameter(&i.inst, 0, C.ulong(p), C.ulonglong(x), nil)

  case string:
    str := C.CString(x)
    defer C.free(unsafe.Pointer(str))
    stat = C.JetSetSystemParameter(&i.inst, 0, C.ulong(p), 0, str)

  default:
    // TODO: should return a JetError instead.
    panic("unexpected parameter type")
  }

  return JetError(stat)
}

func (i *Instance) Initialize(flags ...JetGrbit) JetError {
  stat := C.JetInit2(&i.inst, jetSumGrbits(flags))
  return JetError(stat)
}

func (i *Instance) Terminate(flags ...JetGrbit) JetError {
  stat := C.JetTerm2(i.inst, jetSumGrbits(flags))
  return JetError(stat)
}

func (i *Instance) BackupTo(dir string) JetError {
  dir_ := C.CString(dir)
  defer C.free(unsafe.Pointer(dir_))
  return JetError(C.JetBackupInstance(i.inst, dir_, 0, nil))
}
