// +build windows

package ese

// #cgo LDFLAGS: -lesent
// #include <stdlib.h>
// #include <esent.h>
import "C"

// import "unsafe"

type Session struct {
  sesid C.JET_SESID
}

func (i *Instance) BeginSession() (*Session, JetError) {
  var ses C.JET_SESID
  stat := C.JetBeginSession(i.inst, &ses, nil, nil)
  return &Session{ ses }, JetError(stat)
}

func (s *Session) End() JetError {
  return JetError(C.JetEndSession(s.sesid, 0))
}

func (s *Session) BeginTransaction(flags ...JetGrbit) JetError {
  return JetError(C.JetBeginTransaction2(s.sesid, jetSumGrbits(flags)))
}

func (s *Session) CommitTransaction(flags ...JetGrbit) JetError {
  return JetError(C.JetCommitTransaction(s.sesid, jetSumGrbits(flags)))
}

func (c *Session) RollbackTransaction(flags ...JetGrbit) JetError {
  return JetError(C.JetRollback(s.sesid, jetSumGrbits(flags)))
}
