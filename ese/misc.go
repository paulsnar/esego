// +build windows

package ese

// TODO

const (
  JetMoveFirst uint32 = 1 << 31
  JetMoveLast = ^JetMoveFirst
  JetMoveNext = 1
  JetMovePrevious = ^JetMoveNext
)

const (
  JetDBIDNil uint32 = 0xFFFFFFFF
)

// JET_CONDITIONALCOLUMN
const (
  JetBitIndexColumnMustBeNull JetGrbit = 1 << iota
  JetBitIndexColumnMustBeNotNull
)

// JET_INDEXRANGE
const (
  JetBitRecordInIndex JetGrbit = 1 << iota
  JetBitRecordNotInIndex
)

// Index key definitions
const (
  JetBitKeyAscending JetGrbit = iota
  JetBitKeyDescending JetGrbit = 1 << (iota - 1)
)

// Table classes
const (
  _ JetGrbit = iota << 16
  JetBitTableClass1
  JetBitTableClass2
  JetBitTableClass3
  JetBitTableClass4
  JetBitTableClass5
  JetBitTableClass6
  JetBitTableClass7
  JetBitTableClass8
  JetBitTableClass9
  JetBitTableClass10
  JetBitTableClass11
  JetBitTableClass12
  JetBitTableClass13
  JetBitTableClass14
  JetBitTableClass15
  JetBitTableClassMask = 0xF << 16
  JetBitTableClassNone = 0
)

const JetLSNil uint32 = 0

const (
  JetPrepInsert int = 0
  JetPrepReplace = 2
  JetPrepCancel = 3
  JetPrepReplaceNoLock = 4
  JetPrepInsertCopy = 5
  JetPrepInsertCopyDeleteOriginal = 7
)

// Compact options
const (
  _ JetGrbit = 1 << iota
  _
  _
  _
  _
  JetBitCompactStats
  JetBitCompactRepair
)

// Online defrag options
const (
  JetOnlineDefragDisable JetGrbit = 0
  _ = 1 << (iota - 1)
  JetOnlineDefragDatabases
  JetOnlineDefragSpaceTreef
  JetOnlineDefragAll = 0xFFFF
)

const (
  JetExceptionMsgBox uint32 = iota
  JetExceptionNone
)

const (
  JetEventLoggingDisable uint32 = 0
  JevEventLoggingLevelMax = 100
)

const (
  JetDbInfoFilename int = iota
  JetDbInfoConnect
  JetDbInfoCountry
  JetDbInfoLCID // previously JetDbInfoLangid
  JetDbInfoCp
  JetDbInfoCollate
  JetDbInfoOptions
  JetDbInfoTransactions
  JetDbInfoVersion
  JetDbInfoIsam
  JetDbInfoFilesize
  JetDbInfoSpaceOwned
  JetDbInfoSpaceAvailable
  JetDbInfoUpgrade
  JetDbInfoMisc
  JetDbInfoDBInUse
  _
  JetDbInfoPageSize
)
