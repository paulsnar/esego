// +build windows

package ese

// #cgo LDFLAGS: -lesent
// #include <stdlib.h>
// #include <esent.h>
import "C"

import "unsafe"

type Table struct {
  tblid C.JET_TABLEID
  s *Session
}

type TableCreate struct {
  Name string
  ParentTemplate string
  Pages, Density int
  Columns TableCreateColumns
  Indexes TableCreateIndexes
  Flags JetGrbit
}

func (tc *TableCreate) toJetTableCreate2() (jetTc C.JET_TABLECREATE2) {
  jetTc.cbStruct = C.sizeof_JET_TABLECREATE2
  jetTc.szTableName = C.CString(tc.Name)
  if tc.ParentTemplate != "" {
    jetTc.szTemplateTableName = C.CString(tc.ParentTemplate
  }
  jetTc.rgcolumncreate, jetTc.cColumns = tc.Columns.toJetColumnCreate()
  jetTc.rgindexcreate,  jetTc.cIndexes = tc.Indexes.toJetColumnCreate()
  // TODO: implement szCallback
  jetTc.grbit = C.ulong(tc.Flags)
  return
}

const (
  JetColtypNil int = iota
  JetColtypBit
  JetColtypUnsignedByte
  JetColtypShort
  JetColtypLong
  JetColtypCurrency
  JetColtypIEEESingle
  JetColtypIEEEDouble
  JetColtypDateTime
  JetColtypeBinary
  JetColtypText
  JetColtypLongBinary
  JetColtypLongText
  _ // was SLV
  JetColtypUnsignedLong
  JetColtypLongLong
  JetColtypGUID
  JetColtypUnsignedShort
  JetColtypMax // ?
)

type TableCreateColumn struct {
  Name string
  Type JetColtyp
  MaxLength int
  Flags JetGrbit
  Default []byte
  Codepage int
}

type TableCreateIndex struct {
  Name string
  Key []byte
  Flags JetGrbit
  Density, LCID int
  // pidxunicode?

}

func (db *DB) CreateEmptyTable(name string, pages, density int) (*Table, JetError) {
  name_ := C.CString(name)
  defer C.free(unsafe.Pointer(name_))
  pages_ := C.ulong(uint32(pages))
  density_ := C.ulong(uint32(density))

  var tbl C.JET_TABLEID
  stat := C.JetCreateTable(db.s.sesid, db.dbid, name_, pages_, density_, &tbl)
  return &Table{ tbl, db.s }, JetError(stat)
}

func (db *DB) OpenTable(name string, flags ...JetGrbit) (*Table, JetError) {
  name_ := C.CString(name)
  defer C.free(unsafe.Pointer(name_))

  var tbl C.JET_TABLEID
  stat := C.JetOpenTable(
    db.s.sesid, db.dbid, name_, nil, 0, jetSumGrbits(flags), &tbl)
  return &Table{ tbl, db.s }, JetError(stat)
}

func (t *Table) Close() JetError {
  return JetError(C.JetCloseTable(t.s.sesid, t.tblid))
}
