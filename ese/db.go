// +build windows

package ese

// #cgo LDFLAGS: -lesent
// #include <stdlib.h>
// #include <esent.h>
import "C"

import "unsafe"

type DB struct {
  s *Session
  dbid C.JET_DBID
}

func (s *Session) CreateDatabase(name string) (*DB, JetError) {
  var id C.JET_DBID
  name_ := C.CString(name)
  defer C.free(unsafe.Pointer(name_))
  stat := JetError(C.JetCreateDatabase(s.sesid, name_, nil, &id, 0))
  return &DB{ s, id }, stat
}

func (s *Session) AttachDatabase(name string, flags ...JetGrbit) JetError {
  name_ := C.CString(name)
  defer C.free(unsafe.Pointer(name_))

  return JetError(C.JetAttachDatabase(s.sesid, name_, jetSumGrbits(flags)))
}

func (s *Session) DetachDatabase(name string, flags ...JetGrbit) JetError {
  name_ := C.CString(name)
  defer C.free(unsafe.Pointer(name_))

  return JetError(C.JetDetachDatabase2(s.sesid, name_, jetSumGrbits(flags)))
}

func (s *Session) OpenDatabase(name string, flags ...JetGrbit) (*DB, JetError) {
  var id C.JET_DBID

  name_ := C.CString(name)
  defer C.free(unsafe.Pointer(name_))

  return &DB{ s, id }, JetError(
    C.JetOpenDatabase(s.sesid, name_, nil, &id, jetSumGrbits(flags)))
}

func (db *DB) Close() JetError {
  return JetError(C.JetCloseDatabase(db.s.sesid, db.dbid, 0))
}
