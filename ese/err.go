// +build windows

package ese

type JetError int

const (
	JetErrSuccess JetError = 0

	JetWrnRemainingVersions = 321
	JetWrnUniqueKey = 345
	JetWrnSeparateLongValue = 406
	JetWrnExistingLogFileHasBadSignature = 558
	JetWrnExistingLogFileIsNotContiguous = 559
	JetWrnTargetInstanceRunning = 578
	JetWrnDatabaseRepaired = 595
	JetWrnColumnNull = 1004
	JetWrnBufferTruncated = 1006
	JetWrnDatabaseAttached = 1007
	JetWrnSortOverflow = 1009
	JetWrnSeekNotEqual = 1039
	JetWrnRecordFoundGreater = JetWrnSeekNotEqual
	JetWrnRecordFoundLess = JetWrnSeekNotEqual
	JetWrnNoErrorInfo = 1055
	JetWrnNoIdleActivity = 1058
	JetWrnNoWriteLock = 1067
	JetWrnColumnSetNull = 1068
	JetWrnTableEmpty = 1301
	JetWrnTableInUseBySystem = 1327
	JetWrnCorruptIndexDeleted = 1415
	JetWrnColumnMaxTruncated = 1512
	JetWrnColumnSkipped = 1531
	JetWrnColumnNotLocal = 1532
	JetWrnColumnMoreTags = 1533
	JetWrnColumnTruncated = 1534
	JetWrnColumnPresent = 1535
	JetWrnColumnSingleValue = 1536
	JetWrnColumnDefault = 1537
	JetWrnDataHasChanged = 1610
	JetWrnKeyChanged = 1618
	JetWrnFileOpenReadOnly = 1813
	JetWrnIdleFull = 1908
	JetWrnDefragAlreadyRunning = 2000
	JetWrnDefragNotRunning = 2001
	JetWrnCallbackNotRegistered = 2100

	JetWrnNyi = -1
	JetErrRfsFailure = -100
	JetErrRfsNotArmed = -101
	JetErrFileClose = -102
	JetErrOutOfThreads = -103
	JetErrTooManyIO = -105
	JetErrTaskDropped = -106
	JetErrInternalError = -107
	JetErrDatabaseBufferDependenciesCorrupted = -255
	JetErrPreviousVersion = -322
	JetErrPageBoundary = -323
	JetErrKeyBoundary = -324
	JetErrBadPageLink = -327
	JetErrBadBookmark = -328
	JetErrNTSystemCallFailed = -334
	JetErrBadParentPageLink = -338
	JetErrSPAvailExtCacheOutOfSync = -340
	JetErrSPAvailExtCorrupted = -341
	JetErrSPAvailExtCacheOutOfMemory = -342
	JetErrSPOwnExtCorrupted = -343
	JetErrDbTimeCorrupted = -344
	JetErrKeyTruncated = -346
	JetErrKeyTooBig = -408
	JetErrInvalidLoggedOperation = -500
	JetErrLogFileCorrupt = -501
	JetErrNoBackupDirectory = -503
	JetErrBackupDirectoryNotEmpty = -504
	JetErrBackupInProgress = -505
	JetErrRestoreInProgress = -506
	JetErrMissingPreviousLogFile = -509
	JetErrLogWriteFail = -510
	JetErrLogDisabledDueToRecoveryFailure = -511
	JetErrCannotLogDuringRecoveryRedo = -512
	JetErrLogGenerationMismatch = -513
	JetErrBadLogVersion = -514
	JetErrInvalidLogSequence = -515
	JetErrLoggingDisabled = -516
	JetErrLogBufferTooSmall = -517
	JetErrLogSequenceEnd = -519
	JetErrNoBackup = -520
	JetErrInvalidBackupSequence = -521
	JetErrBackupNotAllowedYet = -523
	JetErrDeleteBackupFileFail = -524
	JetErrMakeBackupDirectoryFail = -525
	JetErrInvalidBackup = -526
	JetErrRecoveredWithErrors = -527
	JetErrMissingLogFile = -528
	JetErrLogDiskFull = -529
	JetErrBadLogSignature = -530
	JetErrBadDbSignature = -531
	JetErrBadCheckpointSignature = -532
	JetErrCheckpointCorrupt = -533
	JetErrMissingPatchPage = -534
	JetErrBadPatchPage = -535
	JetErrRedoAbruptEnded = -536
	JetErrPatchFileMissing = -538
	JetErrDatabaseLogSetMismatch = -539
	JetErrLogFileSizeMismatch = -541
	JetErrCheckpointFileNotFound = -542
	JetErrRequiredLogFilesMissing = -543
	JetErrSoftRecoveryOnBackupDatabase = -544
	JetErrLogFileSizeMismatchDatabasesConsistent = -545
	JetErrLogSectorSizeMismatch = -546
	JetErrLogSectorSizeMismatchDatabasesConsistent = -547
	JetErrLogSequenceEndDatabasesConsistent = -548
	JetErrStreamingDataNotLogged = -549
	JetErrDatabaseDirtyShutdown = -550
	JetErrDatabaseInconsistent = JetErrDatabaseDirtyShutdown
	JetErrConsistentTimeMistmatch = -551
	JetErrDatabasePatchFileMismatch = -552
	JetErrEndingRestoreLogTooLow = -553
	JetErrStartingRestoreLogTooHigh = -554
	JetErrGivenLogFileHasBadSignature = -555
	JetErrGivenLogFileIsNotContiguous = -556
	JetErrMissingRestoreLogFiles = -557
	JetErrMissingFullBackup = -560
	JetErrBadBackupDatabaseSize = -561
	JetErrDatabaseAlreadyUpgraded = -562
	JetErrDatabaseIncompleteUpgrade = -563
	JetErrMissingCurrentLogFiles = -565
	JetErrDbTimeTooOld = -566
	JetErrDbTimeTooNew = -567
	JetErrMissingFileToBackup = -569
	JetErrLogTornWriteDuringHardRestore = -570
	JetErrLogTornWriteDuringHardRecovery = -571
	JetErrLogCorruptDuringHardRestore = -573
	JetErrLogCorruptDuringHardRecovery = -574
	JetErrMustDisableLoggingForDbUpgrade = -575
	JetErrBadRestoreTargetInstance = -577
	JetErrRecoveredWithoutUndo = -579
	JetErrDatabasesNotFromSameSnapshot = -580
	JetErrSoftRecoveryOnSnapshot = -581
	JetErrUnicodeTranslationBufferTooSmall = -601
	JetErrUnicodeTranslationFail = -602
	JetErrUnicodeNormalizationNotSupported = -603
	JetErrExistingLogFileHasBadSignature = -610
	JetErrExistingLogFileIsNotContiguous = -611
	JetErrLogReadVerifyFailure = -612
	JetErrCheckpointDepthTooDeep = -614
	JetErrRestoreOfNonBackupDatabase = -615
	JetErrInvalidGrbit = -900
	JetErrTermInProgress = -1000
	JetErrFeatureNotAvailable = -1001
	JetErrInvalidName = -1002
	JetErrInvalidParameter = -1003
	JetErrDatabaseFileReadOnly = -1008
	JetErrInvalidDatabaseId = -1010
	JetErrOutOfMemory = -1011
	JetErrOutOfDatabaseSpace = -1012
	JetErrOutOfCursors = -1013
	JetErrOutOfBuffers = -1014
	JetErrTooManyIndexes = -1015
	JetErrTooManyKeys = -1016
	JetErrRecordDeleted = -1017
	JetErrReadVerifyFailure = -1018
	JetErrPageNotInitialized = -1019
	JetErrOutOfFileHandles = -1020
	JetErrDiskIO = -1022
	JetErrInvalidPath = -1023
	JetErrInvalidSystemPath = -1024
	JetErrInvalidLogDirectory = -1025
	JetErrRecordTooBig = -1026
	JetErrTooManyOpenDatabases = -1027
	JetErrInvalidDatabase = -1028
	JetErrNotInitialized = -1029
	JetErrAlreadyInitialized = -1030
	JetErrInitInProgress = -1031
	JetErrFileAccessDenied = -1032
	JetErrBufferTooSmall = -1038
	JetErrTooManyColumns = -1040
	JetErrContainerNotEmpty = -1043
	JetErrInvalidFilename = -1044
	JetErrInvalidBookmark = -1045
	JetErrColumnInUse = -1046
	JetErrInvalidBufferSize = -1047
	JetErrColumnNotUpdatable = -1048
	JetErrIndexInUse = -1051
	JetErrLinkNotSupported = -1052
	JetErrNullKeyDisallowed = -1053
	JetErrNotInTransaction = -1054
	JetErrTooManyActiveUsers = -1059
	JetErrInvalidCountry = -1061
	JetErrInvalidLanguageId = -1062
	JetErrInvalidCodePage = -1063
	JetErrInvalidLCMapStringFlags = -1064
	JetErrVersionStoreEntryTooBig = -1065
	JetErrVersionStoreOutOfMemoryAndCleanupTimedOut = -1066
	JetErrVersionStoreOutOfMemory = -1069
	JetErrCannotIndex = -1071
	JetErrRecordNotDeleted = -1072
	JetErrTooManyMempoolEntries = -1073
	JetErrOutOfObjectIDs = -1074
	JetErrOutOfLongValueIDs = -1075
	JetErrOutOfAutoincrementValues = -1076
	JetErrOutOfDbtimeValues = -1077
	JetErrOutOfSequentialIndexValues = -1078
	JetErrRunningInOneInstanceMode = -1080
	JetErrRunningInMultiInstanceMode = -1081
	JetErrSystemParamsAlreadySet = -1082
	JetErrSystemPathInUse = -1083
	JetErrLogFilePathInUse = -1084
	JetErrTempPathInUse = -1085
	JetErrInstanceNameInUse = -1086
	JetErrInstanceUnavailable = -1090
	JetErrDatabaseUnavailable = -1091
	JetErrInstanceUnavailableDueToFatalLogDiskFull = -1092
	JetErrOutOfSessions = -1101
	JetErrWriteConflict = -1102
	JetErrTransTooDeep = -1103
	JetErrInvalidSesid = -1104
	JetErrWriteConflictPrimaryIndex = -1105
	JetErrInTransaction = -1108
	JetErrRollbackRequired = -1109
	JetErrTransReadOnly = -1110
	JetErrSessionWriteConflict = -1111
	JetErrRecordTooBigForBackwardCompatibility = -1112
	JetErrCannotMaterializeForwardOnlySort = -1113
	JetErrSesidTableIdMismatch = -1114
	JetErrInvalidInstance = -1115
	JetErrReadLostFlushVerifyFailure = -1119
	JetErrDatabaseDuplicate = -1201
	JetErrDatabaseInUse = -1202
	JetErrDatabaseNotFound = -1203
	JetErrDatabaseInvalidName = -1204
	JetErrDatabaseInvalidPages = -1205
	JetErrDatabaseCorrupted = -1206
	JetErrDatabaseLocked = -1207
	JetErrCannotDisableVersioning = -1208
	JetErrInvalidDatabaseVersion = -1209
	JetErrDatabase200Format = -1210
	JetErrDatabase400Format = -1211
	JetErrDatabase500Format = -1212
	JetErrPageSizeMismatch = -1213
	JetErrTooManyInstances = -1214
	JetErrDatabaseSharingViolation = -1215
	JetErrAttachedDatabaseMismatch = -1216
	JetErrDatabaseInvalidPath = -1217
	JetErrDatabaseIdInUse = -1218
	JetErrForceDetachNotAllowed = -1219
	JetErrCatalogCorrupted = -1220
	JetErrPartiallyAttachedDB = -1221
	JetErrDatabaseSignInUse = -1222
	JetErrDatabaseCorruptedNoRepair = -1224
	JetErrInvalidCreateDbVersion = -1225
	JetErrTableLocked = -1302
	JetErrTableDuplicate = -1303
	JetErrTableInUse = -1304
	JetErrObjectNotFound = -1305
	JetErrDensityInvalid = -1307
	JetErrTableNotEmpty = -1308
	JetErrInvalidTableId = -1310
	JetErrTooManyOpenTables = -1311
	JetErrIllegalOperation = -1312
	JetErrTooManyOpenTablesAndCleanupTimedOut = -1313
	JetErrObjectDuplicate = -1314
	JetErrInvalidObject = -1316
	JetErrCannotDeleteTempTable = -1317
	JetErrCannotDeleteSystemTable = -1318
	JetErrCannotDeleteTemplateTable = -1319
	JetErrExclusiveTableLockRequired = -1322
	JetErrFixedDDL = -1323
	JetErrFixedInheritedDDL = -1324
	JetErrCannotNestDDL = -1325
	JetErrDDLNotInheritable = -1326
	JetErrInvalidSettings = -1328
	JetErrClientRequestToStopJetService = -1329
	JetErrCannotAddFixedVarColumnToDerivedTable = -1330
	JetErrIndexCantBuild = -1401
	JetErrIndexHasPrimary = -1402
	JetErrIndexDuplicate = -1403
	JetErrIndexNotFound = -1404
	JetErrIndexMustStay = -1405
	JetErrIndexInvalidDef = -1406
	JetErrInvalidCreateIndex = -1409
	JetErrTooManyOpenIndexes = -1410
	JetErrMultiValuedIndexViolation = -1411
	JetErrIndexBuildCorrupted = -1412
	JetErrPrimaryIndexCorrupted = -1413
	JetErrSecondaryIndexCorrupted = -1414
	JetErrInvalidIndexId = -1416
	JetErrIndexTuplesSecondaryIndexOnly = -1430
	JetErrIndexTuplesTooManyColumns = -1431
	JetErrIndexTuplesNonUniqueOnly = -1432
	JetErrIndexTuplesTextBinaryColumnsOnly = -1433
	JetErrIndexTuplesVarSegMacNotAllowed = -1434
	JetErrIndexTuplesInvalidLimits = -1435
	JetErrIndexTuplesCannotRetrieveFromIndex = -1436
	JetErrIndexTuplesKeyTooSmall = -1437
	JetErrColumnLong = -1501
	JetErrColumnNoChunk = -1502
	JetErrColumnDoesNotFit = -1503
	JetErrNullInvalid = -1504
	JetErrColumnIllegalNull = JetErrNullInvalid
	JetErrColumnIndexed = -1505
	JetErrColumnTooBig = -1506
	JetErrColumnNotFound = -1507
	JetErrColumnDuplicate = -1508
	JetErrMultiValuedColumnMustBeTagged = -1509
	JetErrColumnRedundant = -1510
	JetErrInvalidColumnType = -1511
	JetErrTaggedNotNULL = -1514
	JetErrNoCurrentIndex = -1515
	JetErrKeyIsMade = -1516
	JetErrBadColumnId = -1517
	JetErrBadItagSequence = -1518
	JetErrColumnInRelationship = -1519
	JetErrCannotBeTagged = -1521
	JetErrDefaultValueTooBig = -1524
	JetErrMultiValuedDuplicate = -1525
	JetErrLVCorrupted = -1526
	JetErrMultiValuedDuplicateAfterTruncation = -1528
	JetErrDerivedColumnCorruption = -1529
	JetErrInvalidPlaceholderColumn = -1530
	JetErrRecordNotFound = -1601
	JetErrRecordNoCopy = -1602
	JetErrNoCurrentRecord = -1603
	JetErrRecordPrimaryChanged = -1604
	JetErrKeyDuplicate = -1605
	JetErrAlreadyPrepared = -1607
	JetErrKeyNotMade = -1608
	JetErrUpdateNotPrepared = -1609
	JetErrDataHasChanged = -1611
	JetErrLanguageNotSupported = -1619
	JetErrTooManySorts = -1701
	JetErrInvalidOnSort = -1702
	JetErrTempFileOpenError = -1803
	JetErrTooManyAttachedDatabases = -1805
	JetErrDiskFull = -1808
	JetErrPermissionDenied = -1809
	JetErrFileNotFound = -1811
	JetErrFileInvalidType = -1812
	JetErrAfterInitialization = -1850
	JetErrLogCorrupted = -1852
	JetErrInvalidOperation = -1906
	JetErrAccessDenied = -1907
	JetErrTooManySplits = -1909
	JetErrSessionSharingViolation = -1910
	JetErrEntryPointNotFound = -1911
	JetErrSessionContextAlreadySet = -1912
	JetErrSessionContextNotSetByThisThread = -1913
	JetErrSessionInUse = -1914
	JetErrRecordFormatConversionFailed = -1915
	JetErrOneDatabasePerSession = -1916
	JetErrRollbackError = -1917
	JetErrCallbackFailed = -2101
	JetErrCallbackNotResolved = -2102
	JetErrOSSnapshotInvalidSequence = -2401
	JetErrOSSnapshotTimeOut = -2402
	JetErrOSSnapshotNotAllowed = -2403
	JetErrOSSnapshotInvalidSnapId = -2404
	JetErrLSCallbackNotSpecified = -3000
	JetErrLSAlreadySet = -3001
	JetErrLSNotSet = -3002
	JetErrFileIOSparse = -4000
	JetErrFileIOBeyondEOF = -4001
	JetErrFileCompressed = -4005

	// This flag instructs the JET_ABORTRETRYFAILCALLBACK caller to
	// abort the specified I/O.
	JetErrFileIOAbort = -4002

	// This flag instructs the JET_ABORTRETRYFAILCALLBACK caller to
	// retry the specified I/O.
	JetErrFileIORetry = -4003

	// This flag instructs the JET_ABORTRETRYFAILCALLBACK caller to
	// fail the specified I/O.
	JetErrFileIOFail = -4004


	// For internal use only.
	JetWrnSkipThisRecord = 564

	// For internal use only.
	JetWrnCopyLongValue = 1520

	// Reserved.
	JetErrBadSLVSignature = -538

	// Reserved.
	JetErrDatabaseStreamingFileMismatch = -540

	// Reserved.
	JetErrSLVReadVerifyFailure = -613
)

var jetErrorMessageMap = map[JetError]string{
	JetErrSuccess: "The function succeeded.",
	JetWrnRemainingVersions: "The version store is still active.",
	JetWrnUniqueKey: "A seek on a non-unique index yielded a unique key.",
	JetWrnSeparateLongValue: "A database column is a separated long value.",
	JetWrnExistingLogFileHasBadSignature: "The existing log file has a bad signature.",
	JetWrnExistingLogFileIsNotContiguous: "The existing log file is not contiguous.",
	JetWrnTargetInstanceRunning: "The TargetInstance specified for the restore " +
		"is running.",
	JetWrnDatabaseRepaired: "The database corruption has been repaired.",
	JetWrnColumnNull: "The column has a NULL value.",
	JetWrnBufferTruncated: "The buffer is too small for the data.",
	JetWrnDatabaseAttached: "The database is already attached.",
	JetWrnSortOverflow: "The sort that is being attempted does not " +
		"have enough memory to complete.",
	JetWrnSeekNotEqual: "An exact match was not found during a seek.",
	JetWrnNoErrorInfo: "There is no extended error information.",
	JetWrnNoIdleActivity: "No idle activity occured.",
	JetWrnNoWriteLock: "There is a no write lock at transaction level 0.",
	JetWrnColumnSetNull: "The column is set to a NULL value.",
	JetWrnTableEmpty: "An empty table was opened.",
	JetWrnTableInUseBySystem: "The system cleanup has a cursor open on the table.",
	JetWrnCorruptIndexDeleted: "The out-of-date index must be removed.",
	JetWrnColumnMaxTruncated: "The Max length is too large and has been truncated.",
	JetWrnColumnSkipped: "The column ID or itagSequence was NULL.",
	JetWrnColumnNotLocal: "The column values could not be reconstructed from " +
		"the existing data.",
	JetWrnColumnMoreTags: "The existing column values were not requested for enumeration.",
	JetWrnColumnTruncated: "The column value was truncated.",
	JetWrnColumnPresent: "The column values exist but were not returned by the request.",
	JetWrnColumnSingleValue: "The column value was returned in JET_COLUMNENUM.",
	JetWrnColumnDefault: "The column value is set to the default value of the column.",
	JetWrnDataHasChanged: "The data has changed.",
	JetWrnKeyChanged: "A new key is being used.",
	JetWrnFileOpenReadOnly: "The database file is read only.",
	JetWrnIdleFull: "The idle registry is full.",
	JetWrnDefragAlreadyRunning: "An online defragmentation was already running.",
	JetWrnDefragNotRunning: "An online defragmentation is not running.",
	JetWrnCallbackNotRegistered: "A non-existent callback function was unregistered.",

	JetWrnNyi: "The function is not yet implemented.",
	JetErrRfsFailure: "The Resource Failure Simulator failed.",
	JetErrRfsNotArmed: "The Resource Failure Simulator has not been initialized.",
	JetErrFileClose: "The file could not be closed.",
	JetErrOutOfThreads: "The thread could not be started.",
	JetErrTooManyIO: "The system is busy due to too many IOs.",
	JetErrTaskDropped: "The requested asynchronous task could not be executed.",
	JetErrInternalError: "There was a fatal internal error.",
	JetErrDatabaseBufferDependenciesCorrupted: "The buffer dependencies were set " +
		"improperly and there was a recovery failure.",
	JetErrPreviousVersion: "The version already existed and there was a recovery failure.",
	JetErrPageBoundary: "The page boundary has been reached.",
	JetErrKeyBoundary: "The key boundary has been reached.",
	JetErrBadPageLink: "The database is corrupt.",
	JetErrBadBookmark: "The bookmark has no corresponding address " +
		"in the database.",
	JetErrNTSystemCallFailed: "The call to the operating system failed.",
	JetErrBadParentPageLink: "A parent database is corrupt.",
	JetErrSPAvailExtCacheOutOfSync: "The AvailExt cache does not " +
		"match the B+ tree.",
	JetErrSPAvailExtCorrupted: "The AllAvailExt space tree is corrupt.",
	JetErrSPAvailExtCacheOutOfMemory: "An out of memory error occured while " +
		"allocating an AvailExt cache node.",
	JetErrSPOwnExtCorrupted: "The OwnExt space tree is corrupt.",
	JetErrDbTimeCorrupted: "The Dbtime on the current page is greater than " +
		"the global database dbtime.",
	JetErrKeyTruncated: "An attempt to create a key for an index entry failed " +
		"because the key would have been truncated and the index definition " +
		"disallows key truncation.",
	JetErrKeyTooBig: "The key is too large.",
	JetErrInvalidLoggedOperation: "The logged operation cannot be done.",
	JetErrLogFileCorrupt: "The log file is corrupt.",
	JetErrNoBackupDirectory: "A backup directory was not given.",
	JetErrBackupDirectoryNotEmpty: "The backup directory is not empty.",
	JetErrBackupInProgress: "The backup is active already.",
	JetErrRestoreInProgress: "A restore is in progress.",
	JetErrMissingPreviousLogFile: "The log file is missing for the check point.",
	JetErrLogWriteFail: "There was a failure writing to the log file.",
	JetErrLogDisabledDueToRecoveryFailure: "The attempt to write to the log " +
		"after recovery failed.",
	JetErrCannotLogDuringRecoveryRedo: "The attempt to write to the log " +
		"during the recovery redo failed.",
	JetErrLogGenerationMismatch: "The name of the log file does not match " +
		"the internal generation number.",
	JetErrBadLogVersion: "The version of the log file is not compatible " +
		"with the ESE version.",
	JetErrInvalidLogSequence: "The timestamp in the next log does not " +
		"match the expected timestamp.",
	JetErrLoggingDisabled: "The log is not active.",
	JetErrLogBufferTooSmall: "The log buffer is too small for recovery.",
	JetErrLogSequenceEnd: "The maximum log file number has been exceeded.",
	JetErrNoBackup: "There is no backup in progress.",
	JetErrInvalidBackupSequence: "The backup call is out of sequence.",
	JetErrBackupNotAllowedYet: "A backup cannot be done at this time.",
	JetErrDeleteBackupFileFail: "A backup file could not be deleted.",
	JetErrMakeBackupDirectoryFail: "The backup temporary directory could not " +
		"be created.",
	JetErrInvalidBackup: "Circular logging is enabled; " +
		"an incremental backup cannot be performed.",
	JetErrRecoveredWithErrors: "The data was restored with errors.",
	JetErrMissingLogFile: "The current log file is missing.",
	JetErrLogDiskFull: "The log disk is full.",
	JetErrBadLogSignature: "There is a bad signature for a log file.",
	JetErrBadDbSignature: "There is a bad signature for a database.",
	JetErrBadCheckpointSignature: "There is a bad signature for " +
		"a checkpoint file.",
	JetErrCheckpointCorrupt: "The checkpoint file was not found " +
		"or was corrupt.",
	JetErrMissingPatchPage: "The database patch file page was not found " +
		"or was corrupt.",
	JetErrBadPatchPage: "The database patch file page is not valid.",
	JetErrRedoAbruptEnded: "The redo abruptly ended due to a sudden failure " +
		"while reading logs from the log file.",
	JetErrPatchFileMissing: "The hard restore detected that a database " +
		"patch file is missing from the backup set.",
	JetErrDatabaseLogSetMismatch: "The database does not belong with " +
		"the current set of log files.",
	JetErrLogFileSizeMismatch: "The actual log file size " +
		"does not match JET_paramLogFileSize.",
	JetErrSoftRecoveryOnBackupDatabase: "A soft recovery is about to be used on " +
		"a backup database when a restore should be used instead.",
	JetErrLogFileSizeMismatchDatabasesConsistent: "The databases " +
		"have been recovered, but the log file size used during recovery " +
		"does not match JET_paramLogFileSize.",
	JetErrLogSectorSizeMismatch: "The log file sector size does not match " +
		"the sector size of the current volume.",
	JetErrLogSectorSizeMismatchDatabasesConsistent: "The databases have been " +
		"recovered, but the log file sector size used during recovery does not " +
		"match the sector size of the current volume.",
	JetErrLogSequenceEndDatabasesConsistent: "The databases have been " +
		"recovered, but all possible log generations in the current sequence " +
		"have been used.",
	JetErrStreamingDataNotLogged: "There was an illegal attempt to replay a " +
		"streaming file operation where the data was not logged.",
	JetErrDatabaseDirtyShutdown: "The database was not shutdown cleanly.",
	JetErrConsistentTimeMistmatch: "The last consistent time for the database " +
		"has not been matched.",
	JetErrDatabasePatchFileMismatch: "The database patch file is not generated " +
		"from this backup.",
	JetErrEndingRestoreLogTooLow: "The starting log number is " +
		 "too low for the restore.",
	JetErrStartingRestoreLogTooHigh: "The starting log number is too high " +
		"for the restore.",
	JetErrGivenLogFileHasBadSignature: "The restore log file has a bad signature.",
	JetErrGivenLogFileIsNotContiguous: "The restore log file is not contiguous.",
	JetErrMissingRestoreLogFiles: "Some restore log files are missing.",
	JetErrMissingFullBackup: "The database missed a previous full backup " +
		"before attempting to perform an incremental backup.",
	JetErrBadBackupDatabaseSize: "The backup database size " +
		"is not a multiple of the database page size.",
	JetErrDatabaseAlreadyUpgraded: "The current attempt to upgrade a database " +
		"has been stopped because the database is already current.",
	JetErrDatabaseIncompleteUpgrade: "The database was only partially " +
		"converted to the current format.",
	JetErrMissingCurrentLogFiles: "Some current log files are missing " +
		"for continuous restore.",
	JetErrDbTimeTooOld: "The dbtime on a page is smaller than the dbtimeBefore " +
		"that is on the record.",
	JetErrDbTimeTooNew: "The dbtime on a page is in advance of the " +
		"dbtimeBefore that is in the record.",
	JetErrMissingFileToBackup: "Some log or database patch files were " +
		"missing during the backup.",
	JetErrLogTornWriteDuringHardRestore: "A torn write was detected in " +
		"a backup that was set during a hard restore.",
	JetErrLogTornWriteDuringHardRecovery: "A torn write was detected " +
		"during hard recovery where the log was not part of a backup set.",
	JetErrLogCorruptDuringHardRestore: "Corruption was detected in " +
		"a backup set during a hard restore.",
	JetErrLogCorruptDuringHardRecovery: "Corruption was detected during " +
		"hard recovery where the log was not part of a backup set.",
	JetErrMustDisableLoggingForDbUpgrade: "Logging cannot be enabled while " +
		"attempting to upgrade a database.",
	JetErrBadRestoreTargetInstance: "Either the TargetInstance that was " +
		"specified for restore has not been found or the log files do not match.",
	JetErrRecoveredWithoutUndo: "The database engine successfully replayed all " +
		"operations in the transaction log to perform a crash recovery but the caller " +
		"elected to stop recovery without rolling back uncommitted updates.",
	JetErrDatabasesNotFromSameSnapshot: "The databases to be restored are not from " +
		"the same shadow copy backup.",
	JetErrSoftRecoveryOnSnapshot: "There is a soft recovery on a database from " +
		"a shadow copy backup set.",
	JetErrUnicodeTranslationBufferTooSmall: "The Unicode translation buffer " +
		"is too small.",
	JetErrUnicodeTranslationFail: "The Unicode normalization failed.",
	JetErrUnicodeNormalizationNotSupported: "The operating system does not " +
		"provide support for Unicode normalization and a normalization callback " +
		"was not specified.",
	JetErrExistingLogFileHasBadSignature: "The existing log file has a bad signature.",
	JetErrExistingLogFileIsNotContiguous: "An existing log file is not contiguous.",
	JetErrLogReadVerifyFailure: "A checksum error was found in the log file " +
		"during backup.",
	JetErrCheckpointDepthTooDeep: "There are too many outstanding generations " +
		"between the checkpoint and the current generation.",
	JetErrRestoreOfNonBackupDatabase: "A hard recovery was attempted on a database " +
		"that was not a backup database.",
	JetErrInvalidGrbit: "There is an invalid grbit parameter.",
	JetErrTermInProgress: "Termination is in progress.",
	JetErrFeatureNotAvailable: "This API element is not supported.",
	JetErrInvalidName: "An invalid name is being used.",
	JetErrInvalidParameter: "An invalid API parameter is being used.",
	JetErrDatabaseFileReadOnly: "There was an attempt to attach to a read-only " +
		"database file for read/write operations.",
	JetErrInvalidDatabaseId: "There is an invalid database ID.",
	JetErrOutOfMemory: "The system is out of memory.",
	JetErrOutOfDatabaseSpace: "The maximum database size has been reached.",
	JetErrOutOfCursors: "The table is out of cursors.",
	JetErrOutOfBuffers: "The database is out of page buffers.",
	JetErrTooManyIndexes: "There are too many indexes.",
	JetErrTooManyKeys: "There are too many columns in an index.",
	JetErrRecordDeleted: "The record has been deleted.",
	JetErrReadVerifyFailure: "There is a checksum error on a database page.",
	JetErrPageNotInitialized: "There is a blank database page.",
	JetErrOutOfFileHandles: "There are no file handles.",
	JetErrDiskIO: "There is a disk IO error.",
	JetErrInvalidPath: "There is an invalid file path.",
	JetErrInvalidSystemPath: "There is an invalid system path.",
	JetErrInvalidLogDirectory: "There is an invalid log directory.",
	JetErrRecordTooBig: "The record is larger than maximum size.",
	JetErrTooManyOpenDatabases: "There are too many open databases.",
	JetErrInvalidDatabase: "This is not a database file.",
	JetErrNotInitialized: "The database engine has not been initialized.",
	JetErrAlreadyInitialized: "The database engine is already initialized.",
	JetErrInitInProgress: "The database engine is being initialized.",
	JetErrFileAccessDenied: "The file cannot be accessed because " +
		"the file is locked or in use.",
	JetErrBufferTooSmall: "The buffer is too small.",
	JetErrTooManyColumns: "Too many columns are defined.",
	JetErrContainerNotEmpty: "The container is not empty.",
	JetErrInvalidFilename: "The filename is invalid.",
	JetErrInvalidBookmark: "There is an invalid bookmark.",
	JetErrColumnInUse: "The column used is in an index.",
	JetErrInvalidBufferSize: "The data buffer does not match the column size.",
	JetErrColumnNotUpdatable: "The column value cannot be set.",
	JetErrIndexInUse: "The index is in use.",
	JetErrLinkNotSupported: "The link support is unavailable.",
	JetErrNullKeyDisallowed: "Null keys are not allowed on an index.",
	JetErrNotInTransaction: "The operation must occur within a transaction.",
	JetErrTooManyActiveUsers: "There are too many active database users.",
	JetErrInvalidCountry: "There is an invalid or unknown country code.",
	JetErrInvalidLanguageId: "There is an invalid or unknown language ID.",
	JetErrInvalidCodePage: "There is an invalid or unknown code page.",
	JetErrInvalidLCMapStringFlags: "There are invalid flags being used for LCMapString.",
	JetErrVersionStoreEntryTooBig: "There was an attempt to create " +
		"a version store entry (RCE) that was larger than a version bucket.",
	JetErrVersionStoreOutOfMemoryAndCleanupTimedOut: "The version store is " +
		"out of memory and the cleanup attempt failed to complete.",
	JetErrVersionStoreOutOfMemory: "The version store is out of memory and " +
		"a cleanup was already attempted.",
	JetErrCannotIndex: "The escrow and SLV columns cannot be indexed.",
	JetErrRecordNotDeleted: "The record has not been deleted.",
	JetErrTooManyMempoolEntries: "Too many mempool entries have been requested.",
	JetErrOutOfObjectIDs: "The database is out of B+ tree ObjectIDs.",
	JetErrOutOfLongValueIDs: "The Long-value ID counter has reached the maximum value.",
	JetErrOutOfAutoincrementValues: "The auto-increment counter has reached " +
		"the maximum value.",
	JetErrOutOfDbtimeValues: "The Dbtime counter has reached the maximum value.",
	JetErrOutOfSequentialIndexValues: "A sequential index counter has reached " +
		"the maximum value.",
	JetErrRunningInOneInstanceMode: "This multi-instance call has " +
		"the single-instance mode enabled.",
	JetErrRunningInMultiInstanceMode: "This single-instance call has " +
		"the multi-instance mode enabled.",
	JetErrSystemParamsAlreadySet: "The global system parameters have already been set.",
	JetErrSystemPathInUse: "The system path is already being used by " +
		"another database instance.",
	JetErrLogFilePathInUse: "The log file path is already being used by " +
		"another database instance.",
	JetErrTempPathInUse: "The path to the temporary database is already being used by " +
		"another database instance.",
	JetErrInstanceNameInUse: "The instance name is already in use.",
	JetErrInstanceUnavailable: "This instance cannot be used because it encountered " +
		"a fatal error.",
	JetErrDatabaseUnavailable: "This database cannot be used because it encountered " +
		"a fatal error.",
	JetErrInstanceUnavailableDueToFatalLogDiskFull: "This instance annot be used " +
		"because it encountered a log-disk-full error while performing an operation " +
		"that could not tolerate failure.",
	JetErrOutOfSessions: "The database is out of sessions.",
	JetErrWriteConflict: "The write lock failed due to the existence of an " +
		"outstanding write lock.",
	JetErrTransTooDeep: "The transactions are nested too deeply.",
	JetErrInvalidSesid: "There is an invalid session handle.",
	JetErrWriteConflictPrimaryIndex: "An update was attempted on an uncommitted " +
		"primary index.",
	JetErrInTransaction: "The operation is not allowed within a transaction.",
	JetErrRollbackRequired: "The current transaction must be rolled back.",
	JetErrTransReadOnly: "A read-only transaction tried to modify the database.",
	JetErrSessionWriteConflict: "There was an attempt to replace the same record " +
		"by two different cursors in the same session.",
	JetErrRecordTooBigForBackwardCompatibility: "The record would be too big if " +
		"represented in a database format from a previous version of Jet.",
	JetErrCannotMaterializeForwardOnlySort: "The temporary table could not be created " +
		"due to parameters that conflict with JET_bitTTForwardOnly.",
	JetErrSesidTableIdMismatch: "The session handle cannot be used with the table id " +
		"because it was not used to create it.",
	JetErrInvalidInstance: "The instance handle is invalid or refers to an instance " +
		"that has been shut down.",
	JetErrReadLostFlushVerifyFailure: "The database page read from disk " +
		"had a previous write not represented on the page.",
	JetErrDatabaseDuplicate: "The database already exists.",
	JetErrDatabaseInUse: "The database is in use.",
	JetErrDatabaseNotFound: "There is no such database.",
	JetErrDatabaseInvalidName: "The database name is invalid.",
	JetErrDatabaseInvalidPages: "There are an invalid number of pages.",
	JetErrDatabaseCorrupted: "There is a non-database file or corrupt database.",
	JetErrDatabaseLocked: "The database is exclusively locked.",
	JetErrCannotDisableVersioning: "The versioning for this database cannot be disabled.",
	JetErrInvalidDatabaseVersion: "The database engine is incompatible with the database.",
	JetErrDatabase200Format: "The database is in an older (200) format.",
	JetErrDatabase400Format: "The database is in an older (400) format.",
	JetErrDatabase500Format: "The database is in an older (500) format.",
	JetErrPageSizeMismatch: "The database page size does not match the engine.",
	JetErrTooManyInstances: "No more database instances can be started.",
	JetErrDatabaseSharingViolation: "A different database instance is using " +
		"this database.",
	JetErrAttachedDatabaseMismatch: "An outstanding database attachment has been " +
		"detected at the start or end of the recovery, but the database is missing " +
		"or does not match attachment info.",
	JetErrDatabaseInvalidPath: "The specified path to the database file is illegal.",
	JetErrDatabaseIdInUse: "A database is being assigned an ID that is already in use.",
	JetErrForceDetachNotAllowed: "The force detach is allowed only after the " +
		"normal detach was stopped due to an error.",
	JetErrCatalogCorrupted: "Corruption was detected in the catalog.",
	JetErrPartiallyAttachedDB: "The database is only partially attached and " +
		"the attach operation cannot be completed.",
	JetErrDatabaseSignInUse: "The database with the same signature is already in use.",
	JetErrDatabaseCorruptedNoRepair: "The database is corrupted but a repair is " +
		"not allowed.",
	JetErrInvalidCreateDbVersion: "The database engine attempted to replay a " +
		"Create Database operation from the transaction log but failed due to an " +
		"incompatible version of that operation.",
	JetErrTableLocked: "The table is exclusively locked.",
	JetErrTableDuplicate: "The table already exists.",
	JetErrTableInUse: "The table is in use and cannot be locked.",
	JetErrObjectNotFound: "There is no such table or object.",
	JetErrDensityInvalid: "There is a bad file or index density.",
	JetErrTableNotEmpty: "The table is not empty.",
	JetErrInvalidTableId: "The table ID is invalid.",
	JetErrTooManyOpenTables: "No more tables can be opened, even after the " +
		"internal cleanup task has run.",
	JetErrIllegalOperation: "The operation is not supported on the table.",
	JetErrTooManyOpenTablesAndCleanupTimedOut: "No more tables can be opened " +
		"because the cleanup attempt failed to complete.",
	JetErrObjectDuplicate: "The table or object name is in use.",
	JetErrInvalidObject: "The object is invalid for operation.",
	JetErrCannotDeleteTempTable: "JetCloseTable must be used instead of " +
		"JetDeleteTable to delete a temporary table.",
	JetErrCannotDeleteSystemTable: "There was an illegal attempt to delete a " +
		"system table.",
	JetErrCannotDeleteTemplateTable: "There was an illegal attempt to delete a " +
		"template table.",
	JetErrExclusiveTableLockRequired: "There must be an exclusive lock on the table.",
	JetErrFixedDDL: "DDL operations are prohibited on this table.",
	JetErrFixedInheritedDDL: "On a derived table, DDL operations are prohibited " +
		"on the inherited portion of the DDL.",
	JetErrCannotNestDDL: "Nesting the hierarchical DDL is not currently supported.",
	JetErrDDLNotInheritable: "There was an attempt to inherit a DDL from a table " +
		"that is not marked as a template table.",
	JetErrInvalidSettings: "The system parameters were set improperly.",
	JetErrClientRequestToStopJetService: "The client has requested that " +
		"the service be stopped.",
	JetErrCannotAddFixedVarColumnToDerivedTable: "The Template table was created " +
		"with the NoFixedVarColumnsInDerivedTables flag set.",
	JetErrIndexCantBuild: "The index build failed.",
	JetErrIndexHasPrimary: "The primary index is already defined.",
	JetErrIndexDuplicate: "The index is already defined.",
	JetErrIndexNotFound: "There is no such index.",
	JetErrIndexMustStay: "The clustered index cannot be deleted.",
	JetErrIndexInvalidDef: "The index definition is invalid.",
	JetErrInvalidCreateIndex: "The creation of the index description was invalid.",
	JetErrTooManyOpenIndexes: "The database is out of index description blocks.",
	JetErrMultiValuedIndexViolation: "Non-unique inter-record index keys have been " +
		"generated for a multi-valued index.",
	JetErrIndexBuildCorrupted: "A secondary index that properly reflects the " +
		"primary index failed to build.",
	JetErrPrimaryIndexCorrupted: "The primary index is corrupt and the " +
		"database must be defragmented.",
	JetErrSecondaryIndexCorrupted: "The secondary index is corrupt and the " +
		"database must be defragmented.",
	JetErrInvalidIndexId: "The index ID is invalid.",
	JetErrIndexTuplesSecondaryIndexOnly: "The tuple index can only be set " +
		"on a secondary index.",
	JetErrIndexTuplesTooManyColumns: "The index definition for the tuple index " +
		"contains more key columns than the database engine can support.",
	JetErrIndexTuplesNonUniqueOnly: "The tuple index must be a non-unique index.",
	JetErrIndexTuplesTextBinaryColumnsOnly: "A tuple index definition can only " +
		"contain key columns that have text or binary column types.",
	JetErrIndexTuplesVarSegMacNotAllowed: "The tuple index does not allow setting " +
		"cbVarSegMac.",
	JetErrIndexTuplesInvalidLimits: "The minimum/maximum tuple length or the " +
		"maximum number of characters that are specified for an index are invalid.",
	JetErrIndexTuplesCannotRetrieveFromIndex: "JetRetrieveColumn cannot be called " +
		"with the JET_bitRetrieveFromIndex flag set while retrieving a column on a " +
		"tuple index.",
	JetErrIndexTuplesKeyTooSmall: "The specified key does not meet the " +
		"minimum tuple length.",
	JetErrColumnLong: "The column value is long.",
	JetErrColumnNoChunk: "There is no such chunk in a long value.",
	JetErrColumnDoesNotFit: "The field will not fir in the record.",
	JetErrNullInvalid: "Null is not valid.",
	JetErrColumnIndexed: "The column is indexed and cannot be deleted.",
	JetErrColumnTooBig: "The field length is greater than maximum allowed length.",
	JetErrColumnNotFound: "There is no such column.",
	JetErrColumnDuplicate: "The field is already defined.",
	JetErrMultiValuedColumnMustBeTagged: "An attempt was made to create a " +
		"multi-valued column, but the column was not tagged.",
	JetErrColumnRedundant: "There was a second auto-increment or version column.",
	JetErrInvalidColumnType: "The column data type is invalid.",
	JetErrTaggedNotNULL: "There are no non-NULL tagged columns.",
	JetErrNoCurrentIndex: "The database is invalid because it does not contain " +
		"a current index.",
	JetErrKeyIsMade: "The key is completely made.",
	JetErrBadColumnId: "The column ID is incorrect.",
	JetErrBadItagSequence: "There is a bad itagSequence for the tagged column.",
	JetErrColumnInRelationship: "A column cannot be deleted because it is part of a " +
		"relationship.",
	JetErrCannotBeTagged: "The auto increment and version cannot be tagged.",
	JetErrDefaultValueTooBig: "The default value exceeds the maximum size.",
	JetErrMultiValuedDuplicate: "A duplicate value was detected on a " +
		"unique multi-valued column.",
	JetErrLVCorrupted: "Corruption was encountered in a long-value tree.",
	JetErrMultiValuedDuplicateAfterTruncation: "A duplicate value was detected on a " +
		"unique multi-valued column after the data was normalized, and it is normalizing " +
		"truncated the data before comparison.",
	JetErrDerivedColumnCorruption: "There is an invalid column in derived table.",
	JetErrInvalidPlaceholderColumn: "An attempt was made to convert a column to a " +
		"primary index placeholder, but the column does not meet the necessary criteria.",
	JetErrRecordNotFound: "The key was not found.",
	JetErrRecordNoCopy: "There is no working buffer.",
	JetErrNoCurrentRecord: "There is no current record.",
	JetErrRecordPrimaryChanged: "The primary key might not change.",
	JetErrKeyDuplicate: "There is an illegal duplicate key.",
	JetErrAlreadyPrepared: "An attempt was made to update a record while a " +
		"record update was already in progress.",
	JetErrKeyNotMade: "A call was not made to JetMakeKey.",
	JetErrUpdateNotPrepared: "A call was not made to JetPrepareUpdate.",
	JetErrDataHasChanged: "The data has changed and the operation was aborted.",
	JetErrLanguageNotSupported: "This Windows installation does not support " +
		"the selected language.",
	JetErrTooManySorts: "There are too many sort processes.",
	JetErrInvalidOnSort: "An invalid operation occurred during a sort.",
	JetErrTempFileOpenError: "The temporary file could not be opened.",
	JetErrTooManyAttachedDatabases: "Too many databases are open.",
	JetErrDiskFull: "There is no space left on disk.",
	JetErrPermissionDenied: "Permission is denied.",
	JetErrFileNotFound: "The file was not found.",
	JetErrFileInvalidType: "The file type is invalid.",
	JetErrAfterInitialization: "A restore cannot be started after initialization.",
	JetErrLogCorrupted: "The logs could not be interpreted.",
	JetErrInvalidOperation: "The operation is invalid.",
	JetErrAccessDenied: "Access is denied.",
	JetErrTooManySplits: "An infinite split.",
	JetErrSessionSharingViolation: "Multiple threads are using the same session.",
	JetErrEntryPointNotFound: "An entry point in a required DLL could not be found.",
	JetErrSessionContextAlreadySet: "The specified session already has a " +
		"session context set.",
	JetErrSessionContextNotSetByThisThread: "An attempt was made to reset the " +
		"session context, but the current thread was not the original one that set the " +
		"session context.",
	JetErrSessionInUse: "An attempt was made to terminate the session currently in use.",
	JetErrRecordFormatConversionFailed: "An internal error occured during a " +
		"dynamic record format conversion.",
	JetErrOneDatabasePerSession: "Only one open user database per session is allowed.",
	JetErrRollbackError: "There was an error during rollback.",
	JetErrCallbackFailed: "A callback function call failed.",
	JetErrCallbackNotResolved: "A callback function could not be found.",
	JetErrOSSnapshotInvalidSequence: "The operating system shadow copy API " +
		"was used in an invalid sequence.",
	JetErrOSSnapshotTimeOut: "The operating system shadow copy ended with a time-out.",
	JetErrOSSnapshotNotAllowed: "The operating system shadow copy is not allowed " +
		"because a backup or recovery is in progress.",
	JetErrOSSnapshotInvalidSnapId: "The operation failed because the specified " +
		"operating system shadow copy handle was invalid.",
	JetErrLSCallbackNotSpecified: "An attempt was made to use local storage " +
		"without a callback function being specified.",
	JetErrLSAlreadySet: "An attempt was made to set the local storage for an object " +
		"which already had it set.",
	JetErrLSNotSet: "An attempt was made to retrieve local storage from an object " +
		"which did not have it set.",
	JetErrFileIOSparse: "An I/O operation failed because it was attempted against an " +
		"unallocated region of a file.",
	JetErrFileIOBeyondEOF: "A read was issued to a location beyond the EOF.",
	JetErrFileCompressed: "Read/write access is not supported on compressed files.",
}

var jetErrorAuxMessageMap = map[JetError]string{
	JetErrLogSequenceEndDatabasesConsistent: "All log files and " +
		"the checkpoint file must be deleted and databases must be " +
		"backed up before continuing.",
	JetErrStreamingDataNotLogged: "This is probably caused by an " +
		"attempt to rollforward with circular logging enabled.",
	JetErrDatabaseDirtyShutdown: "A recovery must first be run " +
		"to properly complete database operations for the previous shutdown.",
	JetErrDatabaseIncompleteUpgrade: "The database must be restored from backup.",
	JetErrOutOfObjectIDs: "An online defragmentation must be performed to " +
		"reclaim freed or unused ObjectIDs.",
	JetErrOutOfLongValueIDs: "An offline defragmentation must be performed to " +
		"reclaim free or unused LongValueIDs.",
	JetErrOutOfAutoincrementValues: "An offline defragmentation will not be " +
		"able to reclaim free or unused auto-increment values.",
	JetErrOutOfDbtimeValues: "An offline defragmentation must be performed to " +
		"reclaim free or unused Dbtime values.",
	JetErrOutOfSequentialIndexValues: "An offline defragmentation must be performed " +
		"to reclaim free or unused SequentialIndex values.",
	JetErrRollbackRequired: "The current transaction cannot be committed " +
		"and a new one cannot be started.",
	JetErrOneDatabasePerSession: "This is indicated by setting the " +
		"JET_paramOneDatabasePerSession flag during database creation.",
}

func (e JetError) IsError() bool {
	return e < 0
}

func (e JetError) String() string {
	return jetErrorMessageMap[e]
}

func (e JetError) AuxMessage() string {
	return jetErrorAuxMessageMap[e]
}
