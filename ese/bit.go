// +build windows

package ese

import "C"

type JetGrbit uint32

// Thanks to @fakechris on Github because MinGW's esent.h is missing all
// of this, as is MS's docs.
// https://github.com/fakechris/esent/blob/3dd02df340/esent/esent.h

// JetInit2
const (
  _ JetGrbit = 1 << iota
  _
  JetBitReplayIgnoreMissingDb
)

// JetTerm2
const (
  JetBitTermComplete JetGrbit = 1 << iota
  JetBitTermAbrupt
  JetBitTermStopBackup
)

// JetIdle
const (
  JetBitIdleFlushBuffers JetGrbit = 1 << iota
  JetBitIdleCompact
  JetBitIdleStatus
)

// JetEndSession
const (
)

// JetAttachDatabase, JetOpenDatabase
const (
  JetBitDbReadOnly JetGrbit = 1 << iota
  JetBitDbExclusive
  _
  _
  JetBitDbDeleteCorruptIndexes
  JetBitDbUpgrade = 0x200
  JetBitDbDeleteUnicodeIndexes = 0x400
)

// JetDetachDatabase2
const (
  JetBitForceDetach JetGrbit = 0x1
  JetBitForceCloseAndDetach = 0x3
)

// JetCreateDatabase
const (
  JetBitDbRecoveryOff JetGrbit = 0x8
  JetBitDbShadowingOff = 0x80
  JetBitDbOverwriteExisting = 0x200
)

// JetBackup
const (
  JetBitBackupIncremental JetGrbit = 1 << iota
  _
  JetBitBackupAtomic
  _
  JetBitBackupSnapshot
)

// JetCreateTableColumnIndex
const (
  JetBitTableCreateFixedDDL JetGrbit = 1 << iota
  JetBitTableCreateTemplateTable
  JetBitTableCreateNoFixedVarColumnsInDerivedTables
)

// JetAddColumn, JetGetColumnInfo, JetOpenTempTable
const (
  JetBitColumnFixed JetGrbit = 1 << iota
  JetBitColumnTagged
  JetBitColumnNotNULL
  JetBitColumnVersion
  JetBitColumnAutoincrement
  JetBitColumnUpdatable // JetGetColumnInfo only
  JetBitColumnTTKey // JetOpenTempTable only
  JetBitColumnTTDescending // JetOpenTempTable only
  _
  _
  JetBitColumnMultiValued
  JetBitColumnEscrowUpdate
  JetBitColumnUnversioned
  JetBitColumnMaybeNull
  JetBitColumnFinalize
  JetBitColumnUserDefinedDefault
  _
  JetBitColumnDeleteOnZero
)

// JetDeleteColumn
const (
  JetBitDeleteColumnIgnoreTemplateColumns JetGrbit = 1 << iota
)

// JetSetCurrentIndex
const (
  JetBitMoveFirst JetGrbit = 0x0
  JetBitNoMove = 0x2
)

// JetMakeKey
const (
  JetBitNewKey JetGrbit = 1 << iota
  JetBitStrLimit
  JetBitSubStrLimit
  JetBitNormalizedKey
  JetBitKeyDataZeroLength
  _
  _
  _
  JetBitFullColumnStartLimit
  JetBitFullColumnEndLimit
  JetBitPartialColumnStartLimit
  JetBitPartialColumnEndLimit
)

// JetSetIndexRange
const (
  JetBitRangeInclusive JetGrbit = 1 << iota
  JetBitRangeUpperLimit
  JetBitRagneInstantDuration
  JetBitRangeRemove
)

// JetGetLock
const (
  JetBitReadLock JetGrbit = 1 << iota
  JetBitWriteLock
)

// JetMove
const (
  JetBitMoveKeyNE JetGrbit = 1 << iota
)

// JetSeek
const (
  JetBitSeekEQ JetGrbit = 1 << iota
  JetBitSeekLT
  JetBitSeekLE
  JetBitSeekGE
  JetBitSeekGT
  JetBitSetIndexRange
  JetBitCheckUniqueness
)

// JetGotoSecondaryIndexBookmark
const (
  JetBitBookmarkPermitVirtualCurrency JetGrbit = 1 << iota
)

// JetOpenTable
const (
  JetBitTableDenyWrite JetGrbit = 1 << iota
  JetBitTableDenyRead
  JetBitTableReadOnly
  JetBitTableUpdatable
  JetBitTablePermitDDL
  JetBitTableNoCache
  JetBitTablePreread
)

// ?
const (
  JetBitLSReset JetGrbit = 1 << iota
  JetBitLSCursor
  JetBitLSTable
)

// JetOpenTempTable
const (
  JetBitTTIndexed JetGrbit = 1 << iota
  JetBitTTUnique
  JetBitTTUpdatable
  JetBitTTScrollable
  JetBitTTSortNullsHigh
  JetBitTTForceMaterialization
  JetBitTTForwardOnly
)

// JetSetColumn
const (
  JetBitSetAppendLV JetGrbit = 1 << iota
  _
  JetBitSetOverwriteLV
  JetBitSetSizeLV
  _
  JetBitSetZeroLength
  JetBitSetSeparateLV
  JetBitSetUniqueMultiValues
  JetBitSetUniqueNormalizedMultiValues
  JetBitSetRevertToDefaultValue
)

// JetUpdate
const (
  JetBitUpdateCheckESE97Compatibility JetGrbit = 1 << iota
)

// JetEscrowUpdate
const (
  JetBitEscrowNoRollback JetGrbit = 1 << iota
)

// JetRetrieveColumn
const (
  JetBitRetrieveCopy JetGrbit = 1 << iota
  JetBitRetrieveFromIndex
  JetBitRetrieveFromPrimaryBookmark
  JetBitRetrieveTag
  JetBitRetrieveNull
  JetBitRetrieveIgnoreDefault
  JetBitRetrieveLongId
  JetBitRetrieveLongValueRefCount
)

// JetEnumerateColumns
const (
  JetBitEnumerateCopy JetGrbit = 1 << iota
  _
  _
  _
  _
  JetBitEnumerateIgnoreDefault
  JetBitEnumeratePresenceOnly   = 0x20000
  JetBitEnumerateTaggedOnly     = 0x40000
  JetBitEnumerateCompressOutput = 0x80000
  JetBitEnumerateIgnoreUserDefinedDefault = 0x100000
)

// JetBeginTransaction2
const (
  JetBitTransactionReadOnly JetGrbit = 1 << iota
)

// JetCommitTransaction
const (
  JetBitCommitLazyFlush JetGrbit = 1 << iota
  JetBitWaitLastLevel0Commit
  _
  JetBitWaitAllLevel0Commit
)

// JetRollback
const (
  JetBitRollbackAll JetGrbit = 1 << iota
)

// JET_INDEXCREATE
const (
  JetBitIndexUnique JetGrbit = 1 << iota
  JetBitIndexPrimary
  JetBitIndexDisallowNull
  JetBitIndexIgnoreNull
  _
  JetBitIndexIgnoreAnyNull
  JetBitIndexIgnoreFirstNull
  JetBitIndexLazyFlush
  JetBitIndexEmpty
  JetBitIndexUnversioned
  JetBitIndexSortNullsHigh
  JetBitIndexUnicode
  JetBitIndexTuples
  JetBitIndexTupleLimits
  JetBitIndexCrossProduct
  JetBitIndexKeyMost
  JetBitIndexDisallowTruncation
  JetBitIndexNestedTable
)

func jetSumGrbits(grbits []JetGrbit) C.ulong {
  var out JetGrbit
  for _, flag := range grbits {
    out |= flag
  }
  return C.ulong(out)
}
