package main

import (
	"fmt"
	"ese"
)

func main() {
	jet, err := ese.CreateInstance()
	if err != ese.JetErrSuccess {
		fmt.Printf("create failed: %s\n", err)
		return
	}
	if err := jet.SetSystemParameter(
				ese.JetParamExceptionAction, ese.JetExceptionMsgBox);
			err != ese.JetErrSuccess {
		fmt.Printf("set sys param (exception) failed: %s\n", err)
		return
	}
	if err := jet.SetSystemParameter(ese.JetParamLegacyFileNames, 0);
			err != ese.JetErrSuccess {
		fmt.Printf("set sys param (legacy fn) failed: %s\n", err)
		return
	}
	defer func() {
		if err := jet.Terminate(); err != ese.JetErrSuccess {
			fmt.Printf("[deferred] term failed: %s\n", err)
		}
	}()
	if err := jet.Initialize(); err != ese.JetErrSuccess {
		fmt.Printf("init failed: %s\n", err)
	}

	ses, err := jet.BeginSession()
	if err != ese.JetErrSuccess {
		fmt.Printf("session begin failed: %s\n", err)
		return
	}
	defer func() {
		if err := ses.End(); err != ese.JetErrSuccess {
			fmt.Printf("[deferred] session end failed: %s\n", err)
		}
	}()

	db, err := ses.CreateDatabase("test.db")
	if err != ese.JetErrSuccess {
		if err == ese.JetErrDatabaseDuplicate {
			fmt.Println("@ duplicate, reopening...")
			if err := ses.AttachDatabase("test.db"); err != ese.JetErrSuccess {
				fmt.Printf("attach db failed: %s\n", err)
				return
			}
			defer func() {
				if err := ses.DetachDatabase("test.db"); err != ese.JetErrSuccess {
					fmt.Printf("[deferred] detach db failed: %s\n", err)
				}
			}()
			db, err = ses.OpenDatabase("test.db")
			if err != ese.JetErrSuccess {
				if err.IsError() {
					fmt.Printf("reopen db failed: %s\n", err)
					} else {
						fmt.Printf("reopen db warning: %s\n", err)
					}
				}
		} else {
			fmt.Printf("create db failed: %s\n")
			return
		}
	}
	defer func() {
		if err := db.Close(); err != ese.JetErrSuccess {
			fmt.Printf("[deferred] db close failed: %s\n", err)
		}
	}()

	tbl, err := db.CreateTable("hello", 24, 0)
	if err == ese.JetErrTableDuplicate {
		fmt.Println("@ table duplicate, opening a cursor instead")
		tbl, err = db.OpenTable("hello")
	}
	if err != ese.JetErrSuccess {
		fmt.Printf("create/open tab failed: %s\n", err)
		return
	}
	defer func() {
		if err := tbl.Close(); err != ese.JetErrSuccess {
			fmt.Printf("[deferred] tbl close failed: %s\n", err)
		}
	}()

	// if err := jet.BackupTo("bk_out"); err.IsError() {
	// 	fmt.Printf("backup failed: %s\n", err)
	// } else if err != ese.JetErrSuccess {
	// 	fmt.Printf("backup [warn] %s\n", err)
	// } else {
	// 	fmt.Printf("backup success!")
	// }
}
